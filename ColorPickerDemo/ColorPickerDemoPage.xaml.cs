﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace ColorPickerDemo
{
    public partial class ColorPickerDemoPage : ContentPage
    {
        public ColorPickerDemoPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ColorPickerDemoPage)}:  constructor");

            InitializeComponent();
        }

        void OnPageAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnPageAppearing)}");

            UpdateColorBoxAndLabels();
        }

        void OnColorSliderChanged(object sender, ValueChangedEventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnColorSliderChanged)}");

            UpdateColorBoxAndLabels();
        }

        private void UpdateColorBoxAndLabels()
        {
            double redConvertedAndRounded = Math.Round(RedSlider.Value / 255, 2);
            double greenConvertedAndRounded = Math.Round(GreenSlider.Value / 255, 2);
            double blueConvertedAndRounded = Math.Round(BlueSlider.Value / 255, 2);

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(UpdateColorBoxAndLabels)}:\n\tred={redConvertedAndRounded}\n\tgreen={greenConvertedAndRounded}\n\tblue={blueConvertedAndRounded}");

            UpdateColorBox(redConvertedAndRounded, greenConvertedAndRounded, blueConvertedAndRounded);
            UpdateLabels(redConvertedAndRounded, greenConvertedAndRounded, blueConvertedAndRounded);

            Debug.WriteLine($"\n============================== Update complete! ==============================\n");
        }

        private void UpdateColorBox(double red, double green, double blue)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(UpdateColorBox)}");

            ColorBox.BackgroundColor = new Color(red, green, blue);
        }

        private void UpdateLabels(double red, double green, double blue)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(UpdateLabels)}");

            int redInt = (int)(red * 255);
            int greenInt = (int)(green * 255);
            int blueInt = (int)(blue * 255);

            RedLabel.Text = $"R: {redInt}";
            GreenLabel.Text = $"G: {greenInt}";
            BlueLabel.Text = $"B: {blueInt}";
        }
    }
}
